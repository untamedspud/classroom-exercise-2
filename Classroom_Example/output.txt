$ /tmp/a.out

*** COLORS ARRAY BEFORE ***
[0] = Red.
[1] = Green.
[2] = Blue.
[3] = Purple.
[4] = Brown.

*** PEOPLE ARRAY BEFORE ***
[0] = Ted.
[1] = Mary.
[2] = Karen.
[3] = Susan.
[4] = Robert.

*** COLORS ARRAY AFTER ***
[0] = Red.
[1] = Green.
[2] = Blue.
[3] = Purple.
[4] = Brown.
[5] = Grey.

*** PEOPLE ARRAY AFTER ***
[0] = Ted.
[1] = Mary.
[2] = Karen.
[3] = Susan.
[4] = Robert.
[5] = Tom.

*** COLORS ARRAY AFTER SWAP ***
[0] = Brown.
[1] = Green.
[2] = Blue.
[3] = Purple.
[4] = Red.
[5] = Grey.

*** PEOPLE ARRAY AFTER SWAP ***
[0] = Robert.
[1] = Mary.
[2] = Karen.
[3] = Susan.
[4] = Ted.
[5] = Tom.
$ 
