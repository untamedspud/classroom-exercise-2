#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

/*
 * This code was developed in class to demonstrate how to write the swap driver
 * function required for this exercise.
 *
 * The selection sort code provided in the slide deck for Section Sort can be used
 * with the swap code replaced by this swap fucntion.
 *
 */
 

#define MAXIMUM_STRING_LENGTH 10

void print_str_array( const char *header, const char c[][MAXIMUM_STRING_LENGTH], const int n );
void add_to_array( char c[][MAXIMUM_STRING_LENGTH], char d[][MAXIMUM_STRING_LENGTH], 
	const char *name, const char *color, const int n );
void swap_elements( char c[][MAXIMUM_STRING_LENGTH], char p[][MAXIMUM_STRING_LENGTH], 
		const int loc1, const int loc2 );

int main()
{
	char colors[50][ MAXIMUM_STRING_LENGTH ] = { "Red", "Green", "Blue", "Purple", "Brown" };
	char people[50][ MAXIMUM_STRING_LENGTH ] = { "Ted", "Mary", "Karen", "Susan", "Robert" };
	int number_of_elements = 5;

	print_str_array( "COLORS ARRAY BEFORE", colors, number_of_elements );
	print_str_array( "PEOPLE ARRAY BEFORE", people, number_of_elements );

	add_to_array( people, colors, "Tom", "Grey", number_of_elements );
	number_of_elements++;

	print_str_array( "COLORS ARRAY AFTER", colors, number_of_elements );
	print_str_array( "PEOPLE ARRAY AFTER", people, number_of_elements );

	swap_elements( colors, people, 4, 0 );

	print_str_array( "COLORS ARRAY AFTER SWAP", colors, number_of_elements );
	print_str_array( "PEOPLE ARRAY AFTER SWAP", people, number_of_elements );

	return EXIT_SUCCESS;
}


void swap_elements( char c[][MAXIMUM_STRING_LENGTH], char p[][MAXIMUM_STRING_LENGTH], 
		const int loc1, const int loc2 )
{
	char temp[ MAXIMUM_STRING_LENGTH ];

	strcpy( temp, c[ loc1 ] );
	strcpy( c[ loc1 ], c[ loc2 ] );
	strcpy( c[ loc2 ], temp );

	strcpy( temp, p[ loc1 ] );
	strcpy( p[ loc1 ], p[ loc2 ] );
	strcpy( p[ loc2 ], temp );

	return;
}

void print_str_array( const char *header, const char c[][MAXIMUM_STRING_LENGTH], const int n )
{
	int i;

	printf( "\n*** %s ***\n", header );
	for ( i = 0; i < n; i++ )
		printf( "[%d] = %s.\n", i, c[i] );

	return;
}

void add_to_array( char c[][MAXIMUM_STRING_LENGTH], char d[][MAXIMUM_STRING_LENGTH], 
	const char *name, const char *color, const int n )
{
	strcpy( c[ n ], name ); 
	strcpy( d[ n ], color );

	return;
}
