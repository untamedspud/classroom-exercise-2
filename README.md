# Classroom Exercise #2

Use this data and sample code for your exercise.

This directory contains sample c code for reading column data in from a file.

Use the Pennsylvania county data for your exercise.  Read the data in to your program and store it in parallel arrays.  Then use the Selection Sort to organize the data 2 ways.  First, organized by County name, and second, sorted by population size.

Pennsylvania population data obtained from this web page: https://pasdc.hbg.psu.edu/sdc/pasdc_files/researchbriefs/Apr_2019_CntyEst18.pdf
