#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

// Driver to reading in data from the latest London buildings file
// and printing out the data read in by columns.

#define MAXIMUM_BUILDINGS 30
#define MAXIMUM_STRINGLENGTH_BUILDINGS 24
#define LIMIT_TO_READING_LINES MAXIMUM_BUILDINGS

int processBuildingFile( char fname[], char buildings[][MAXIMUM_STRINGLENGTH_BUILDINGS], int years[] );
void printSingleEntry( const char building[MAXIMUM_STRINGLENGTH_BUILDINGS], const int year );
void printAllBuildings( const char building_names[][MAXIMUM_STRINGLENGTH_BUILDINGS], const int year[], const int number_of_buildings );
void remove_trailing_spaces( char *s );

int main()
{
	char building_names[ MAXIMUM_BUILDINGS ] [ MAXIMUM_STRINGLENGTH_BUILDINGS ];
	int  year[ MAXIMUM_BUILDINGS ];

	int number_of_buildings = processBuildingFile( "london_buildings.txt", building_names, year );   

	printAllBuildings( building_names, year, number_of_buildings  );
	
	puts( "-----" );

	// remove the extra spaces in the string
	int i;
	for ( i = 0; i <= number_of_buildings; i++ )
		remove_trailing_spaces( building_names[ i ] );

	printAllBuildings( building_names, year, number_of_buildings  );

	return EXIT_SUCCESS;
}



int processBuildingFile( char fname[], char buildings[][MAXIMUM_STRINGLENGTH_BUILDINGS], int years[] )
{
	int count = 0;
	FILE *IN = fopen( fname, "r" );

	if ( ! IN ) {
		printf( "Error opening input file %s.\n", fname );
		exit( EXIT_FAILURE );
	}

	while ( fgets( buildings[ count ], MAXIMUM_STRINGLENGTH_BUILDINGS, IN ) != NULL ) {
		fscanf( IN, "%d ", &years[ count ] );
		count++;
		if ( count >= LIMIT_TO_READING_LINES ) 
			break;
	}

	printf( "Number of lines read in = %d.\n\n", count );
	return count;
}


void printSingleEntry( const char building[MAXIMUM_STRINGLENGTH_BUILDINGS], const int year )
{
	printf( "%s was built in the year %d.\n\n", building, year );

	return;
}

void printAllBuildings( const char building_names[][MAXIMUM_STRINGLENGTH_BUILDINGS], const int years[], const int number_of_buildings )
{
	int i;
	for ( i = 0; i < number_of_buildings; i++ )
		printSingleEntry( building_names[ i ], years[ i ] );

	return;
}


void remove_trailing_spaces( char *s )
{
	int i;
	for ( i = strlen( s ) - 1; i > 0; i-- ) {
		if ( ! isspace( s[ i ] ) )
			break;
	}
	s[ i+1 ] = '\0';

	return;
}